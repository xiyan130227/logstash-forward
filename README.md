##logstash-forward

> 转发日志到logstash server 端

### 如何构建

```
git clone https://git.oschina.net/csphere/logstash-forward.git
cd logstash-forward
docker build -t csphere/logstash-forward .
```

### 启动应用容器

```
docker run -d -p 80:80 --name myblog --restart=always -e WORDPRESS_DB_USER=myuser -e WORDPRESS_DB_PASSWORD=mypass -e WORDPRESS_DB_HOST=192.168.42.1 -v /data/logs:/var/log/nginx csphere/wordpress:4.3
```

### 启动logstash-forward

```
docker run -d --name logstash-forward -v /data/logs:/data/logs --link elk:elk csphere/logstash-forward
```
